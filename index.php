<?php

//Load base framework class
//==================================================
require_once 'system/bootstrap.php';

//Initialize the application
//==================================================
new \System\Bootstrap(array(
    'ApplicationName' => 'L4F',
    'ControllerDir' => array(
        'C:\\xampp\\htdocs\\new_l4f\\test_app\\c_dir_a',
        'C:\\xampp\\htdocs\\new_l4f\\test_app\\c_dir_b'
    ),
    'ViewDir' => array(
        'C:\\xampp\\htdocs\\new_l4f\\test_app\v_dir_a',
        'C:\\xampp\\htdocs\\new_l4f\\test_app\\v_dir_b'
    ),
    'ModelDir' => array(
        'C:\\xampp\\htdocs\\new_l4f\\test_app\m_dir_a',
        'C:\\xampp\\htdocs\\new_l4f\\test_app\\m_dir_b'
    ),
    'ConfigDir' => array(
        'C:\\xampp\\htdocs\\new_l4f\\test_app\cfg_a',
        'C:\\xampp\\htdocs\\new_l4f\\test_app\\cfg_b'
    ),
    'LangDir' => array(
        'C:\\xampp\\htdocs\\new_l4f\\test_app\lng_a',
        'C:\\xampp\\htdocs\\new_l4f\\test_app\\lng_b'
    ),
    'Storage' => 'C:\\xampp\\htdocs\\new_l4f\\test_app\s_a',
    'Autoloader' => array(
        'RootDir'   => 'C:\\xampp\\htdocs\\new_l4f\\test_app\lib',
        'Namespace' => array(
            'TestApp', 'FooBar'
        )
    ),
    '3RDLibs' => array(
        'C:\\xampp\\htdocs\\new_l4f\\test_app\\ext'
    )
));