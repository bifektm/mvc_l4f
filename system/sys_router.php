<?php namespace System;

/**
 * Class SYS_Router - MVC Router helper
 * @package System
 */
class SYS_Router{

    /**
     * The raw requested URI
     * @var string|void
     */
    private $_requestedUri = '';

    /**
     * Creates a new instance of the class SYS_Router
     */
    public function __construct(){
        $this -> _requestedUri = $this -> _getRequestUri();

    }

    /**
     * @return array
     */
    public function gerRequestUriArray(){
        return explode('/',substr($this -> _requestedUri,1));
    }
    /**
     * Parse and get the requested URI
     */
    private function _getRequestUri(){
        $raw_uri = false;
        if(isset($_SERVER['REDIRECT_URL'])){
            $raw_uri = $this -> _getRequestUri_fromUrl();
        }
        elseif(isset($_SERVER['REQUEST_URI'])){
            $raw_uri = $this -> _getRequestUri_fromUri();
        }else{
            echo "none";
        }
        return $raw_uri;
    }

    /**
     * Parse and get the requested URI from REDIRECT_URL
     */
    private function _getRequestUri_fromUrl(){
        $raw = $_SERVER['REDIRECT_URL'];
        $sufix = $this -> _getRequestUri_getParentFolder();
        if(strrpos($raw, $sufix, -strlen($raw)) !== false){
            $raw = substr($raw, strlen($sufix));
        }
        return '/'.trim($raw, '\\/');
    }

    /**
     * Parse and get the requested URI from REQUEST_URI
     */
    private function _getRequestUri_fromUri(){
        if(strpos($_SERVER['REQUEST_URI'], '?') !== false){
            $parts = explode('?', $_SERVER['REQUEST_URI']);
            $_SERVER['REDIRECT_URL'] = $parts[0];
            return $this -> _getRequestUri_fromUrl();
        }else{
            $_SERVER['REDIRECT_URL'] = $_SERVER['REQUEST_URI'];
            return $this -> _getRequestUri_fromUrl();
        }
    }

    /**
     * Gets the HTTP server folder
     * @return string
     */
    private function _getRequestUri_getParentFolder(){
        $raw = explode('/', trim(str_replace(array('\\', '/'), '/', $_SERVER['PHP_SELF']), '\\/'));
        $segments = array();
        for($i = count($raw) - 2; $i >= 0; $i--){
            $segments[] = $raw[$i];
        }
        return '/'.implode('/', array_reverse($segments));
    }

}