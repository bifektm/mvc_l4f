<?php namespace System;

/**
 * Class Bootstrap
 * @package System
 */
final class Bootstrap{

    /**
     * Stores all framework core classes
     * @var array
     */
    private static $sys_core = array();

    /**
     * The application name
     * @var string
     */
    private static $appName = '';

    /**
     * The views source dirs
     * @var array
     */
    private static $viewsSrc = array();

    /**
     * The models source dirs
     * @var array
     */
    private static $modelsSrc = array();

    /**
     * The controllers source dirs
     * @var array
     */
    private static $controllersSrc = array();

    /**
     * The lang src dirs
     * @var array
     */
    private static $langSrc = array();

    /**
     * The storage source dirs
     * @var string
     */
    private static $storageTarget = '';

    /**
     * Autoloader config
     * @var array
     */
    private static $autoloader = array();

    /**
     * 3rd libs config
     * @var array
     */
    private static $_3rdLibs = array();
    /**
     * 3rd libs config
     * @var array
     */
    private static $confSrc = array();

    /**
     * @return string
     */
    public static function getAppName(){
        return self::$appName;
    }

    /**
     * @return array
     */
    public static function getViewsSrc(){
        return self::$viewsSrc;
    }

    /**
     * @return array
     */
    public static function getModelsSrc(){
        return self::$modelsSrc;
    }

    /**
     * @return array
     */
    public static function getControllersSrc(){
        return self::$controllersSrc;
    }

    /**
     * @return array
     */
    public static function getLangSrc(){
        return self::$langSrc;
    }

    /**
     * @return string
     */
    public static function getStorageTarget(){
        return self::$storageTarget;
    }

    /**
     * @return array
     */
    public static function getAutoloader(){
        return self::$autoloader;
    }

    /**
     * @return array
     */
    public static function get3rdLibs(){
        return self::$_3rdLibs;
    }

    /**
     * @return array
     */
    public static function getConfSrc(){
        return self::$confSrc;
    }

    /**
     * @return mixed
     */
    public static function getRouter(){
        return self::$sys_core['router'];
    }

    /**
     * Creates a new instance of the class Bpptstrap
     */
    public function __construct($settings = array()){
        if(isset($settings['ApplicationName'])){
            self::$appName = \strval($settings['ApplicationName']);
        }
        $this -> _validateDirSettings($settings);
        foreach($settings['Autoloader']['Namespace'] as $varname){
            if(!preg_match('/^[a-zA-Z_\x7f-\xff][a-zA-Z0-9_\x7f-\xff]*$/', $varname)){
                throw new \Exception($varname.' is not a valid namespace identifier');
            }
        }
        self::$autoloader = $settings['Autoloader'];
        $this -> _registerFrameworkAutoloader();
        $this -> _initCoreClasses();
    }

    /**
     * Initialize the framework core classes
     */
    private function _initCoreClasses(){
        self::$sys_core['router'] = new SYS_Router();
    }

    /**
     * Creates the framework SPL autoloader
     */
    private function _registerFrameworkAutoloader(){
        spl_autoload_register(function($class){
            $class = trim(strtolower($class), "\\/");
            $parts = explode('\\', $class);
            if(!count($parts) >= 2 || $parts[0] !== 'system'){
                return;
            }
            $class = '';
            for($i = 1; $i < count($parts); $i++){
                $class .= $parts[$i].'\\';
            }
            $class = rtrim($class, '\\');
            $file = rtrim(dirname(__FILE__), "\\/").DIRECTORY_SEPARATOR.$class.'.php';
            if(is_file($file) && file_exists($file)){
                /** @noinspection PhpIncludeInspection */
                require_once $file;
            }
        }, true, true);
    }

    /**
     * Validates all the settings relative to the filesystem
     * @param $settings
     * @throws \Exception
     */
    private function _validateDirSettings($settings){
        if(isset($settings['ControllerDir'])){
            self::$controllersSrc = $this -> _validateDirectories($settings['ControllerDir']);
        }else{
            throw new \Exception('ControllerDir not found');
        }
        if(isset($settings['ViewDir'])){
            self::$viewsSrc = $this -> _validateDirectories($settings['ViewDir']);
        }else{
            throw new \Exception('ViewDir not found');
        }
        if(isset($settings['ModelDir'])){
            self::$modelsSrc = $this -> _validateDirectories($settings['ModelDir']);
        }else{
            throw new \Exception('ModelDir not found');
        }
        if(isset($settings['ConfigDir'])){
            self::$confSrc = $this -> _validateDirectories($settings['ConfigDir']);
        }else{
            throw new \Exception('ConfigDir not found');
        }
        if(isset($settings['LangDir'])){
            self::$langSrc = $this -> _validateDirectories($settings['LangDir']);
        }else{
            throw new \Exception('LangDir not found');
        }
        if(isset($settings['Storage'])){
            self::$storageTarget = $this -> _validateDirectory($settings['Storage']);
        }else{
            throw new \Exception('Storage not found');
        }
        if(isset($settings['Autoloader']['RootDir'])){
            $this -> _validateDirectory($settings['Autoloader']['RootDir']);
        }else{
            throw new \Exception('Autoloader->RootDir not found');
        }
        if(isset($settings['3RDLibs'])){
            self::$_3rdLibs = $this -> _validateDirectories($settings['3RDLibs']);
        }else{
            throw new \Exception('3RDLibs not found');
        }
    }

    /**
     * Checks if the given directory list exists
     * @param $dirs
     */
    private function _validateDirectories($dirs){
        foreach($dirs as $dir){
            $this -> _validateDirectory($dir);
        }
        return $dirs;
    }

    /**
     * Checks if the given directory exists
     * @param string $dir
     * @throws \Exception
     * @return bool
     */
    private function _validateDirectory($dir){
        if(!is_dir($dir)){
            throw new \Exception('Directory not exists');

        }
        return $dir;
    }

}